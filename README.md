# Prêt à Pousser "Lilo" plant pot

The plant pot and blank cap are made for the indoor garden "Lilo" made by [Prêt à Pousser][1]. All forms can be printed without any support. Infill of the blank cap can be 10% or even less.

Disclaimer: I'm not affiliated with [Prêt à Pousser][1]. All product and company names are trademarks™ or registered® trademarks of their respective holders. The 3D shapes published here are my own work. I cannot be held responsible for any damage to Prêt à Pousser products caused by the use of my 3D forms. Any use of my 3D forms with Prêt à Pousser products is at your own risk.

Published on [Thingiverse][2] on 18.07.2020.

License: [CC BY-SA 4.0][3]

[1]: https://pretapousser.fr/
[2]: https://www.thingiverse.com/thing:4545815
[3]: https://creativecommons.org/licenses/by-sa/4.0/
