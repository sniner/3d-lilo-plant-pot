// Subject: Prêt à Pousser "Lilo" v2019, https://pretapousser.de/
//
// This 3D model is published under CC BY-SA 4.0 by Stefan Schönberger <mail@sniner.net>

// About presets:
// WALL_THICKNESS: 1.2 mm; slightly thicker for a better print quality, measured: 1.0 mm
// CAGE_WIDTH_TOP: 22.0 mm; reduced for a better fit, measured 22.4 mm
// CAGE_HEIGHT: 58.0 mm; enlarged because of dome shaped bottom, measured: 53.0 mm

/* [Hidden] */
CAP_LENGTH              =  101.2;
CAP_WIDTH               =   24.4;
CAGE_LENGTH             =   90.0;
CAGE_WIDTH_TOP          =   22.0;
CAGE_WIDTH_BOTTOM       =   19.3;
ROUNDING_RADIUS         =    5.0; //[4:9]

/* [Customization] */
WALL_THICKNESS          =    1.2; //[1.0:0.1:2.0]
BOTTOM_OPENINGS         = "some"; //[none,some,more]
GRILLE_HORIZONTAL_BAR   =    2.0; //[1.0:0.1:8.0]
GRILLE_HORIZONTAL_COUNT =      6; //[1:10]
CAGE_HEIGHT             =   58.0; //[55:70]

/* [Model parameter] */
OBJECT_TO_PRINT         =  "pot"; //[pot,cap]
SUPPORT_STRUCTURES      =   true; //[true,false]
ROUNDNESS               =     48; //[24,48,96]

// ---------------------------------------------------------------

function spacing__object_length(length, spacer_length, count) =
    count>0 ? (length - spacer_length*(count-1))/count : length;

module spacing__place_object(length, spacer_length, count, v=[1, 0, 0])
{
    obj_length = spacing__object_length(length, spacer_length, count);
    for (i=[0:count-1]) {
        translate(v*i*(obj_length+spacer_length)) children();
    }
}

module spacing__place_spacer(length, spacer_length, count, v=[1, 0, 0])
{
    if (count>1) {
        obj_length = spacing__object_length(length, spacer_length, count);
        for (i=[0:count-2]) {
            translate(v*(obj_length+i*(obj_length+spacer_length))) children();
        }
    }
}

module spacing__place_spacer_lr(length, spacer_length, count, v=[1, 0, 0])
{
    spacing__place_spacer(length, spacer_length, count, v) children();
    translate(v*(-spacer_length)) children();
    translate(v*length) children();
}

module spacing__place_objects(length, spacer_length, count, v=[1, 0, 0])
{
    obj_length = spacing__object_length(length, spacer_length, count);
    for (i=[0:count-1]) {
        translate(v*i*(obj_length+spacer_length)) children(0);
    }
    if (count>1)
        for (i=[0:count-2]) {
            translate(v*(obj_length+i*(obj_length+spacer_length))) children(1);
        }
}

// ---------------------------------------------------------------

module rounded_plate(length, width, height)
{
    translate([0, 0, -height/2])
    linear_extrude(height)
    hull() {
        translate([-length/2+width/2, 0, 0]) circle(d=width);
        translate([length/2-width/2, 0, 0]) circle(d=width);
    }
}

module cage_form(length, width_top, width_bottom, height, r=5)
{
    hull() {
        // top
        difference() {
            union() {
                translate([length/2-r, width_top/2-r, 0]) sphere(r=r);
                translate([-length/2+r, width_top/2-r, 0]) sphere(r=r);
                translate([length/2-r, -width_top/2+r, 0]) sphere(r=r);
                translate([-length/2+r, -width_top/2+r, 0]) sphere(r=r);
            }
            translate([0, 0, (r+2)/2]) cube([length+2, width_top+2, r+2], center=true);
        }
        // bottom
        r2 = width_bottom/2;
        translate([-length/2+r2, 0, -height+r2]) sphere(r=r2);
        translate([length/2-r2, 0, -height+r2]) sphere(r=r2);
    }
}

module cage_base(
    length=CAGE_LENGTH,
    width_top=CAGE_WIDTH_TOP,
    width_bottom=CAGE_WIDTH_BOTTOM,
    height=CAGE_HEIGHT,
    plate_length=CAP_LENGTH,
    plate_width=CAP_WIDTH,
    r=ROUNDING_RADIUS,
) {
    union() {
        difference() {
            union() {
                cage_form(length, width_top, width_bottom, height, r);
                translate([0, 0, WALL_THICKNESS/2])
                    rounded_plate(length=plate_length, width=plate_width, height=WALL_THICKNESS);
            }
            translate([0, 0, WALL_THICKNESS+0.001])
                cage_form(
                    length=length-2*WALL_THICKNESS,
                    width_top=width_top-2*WALL_THICKNESS,
                    width_bottom=width_bottom-2*WALL_THICKNESS,
                    height=height,
                    r=r-WALL_THICKNESS
                );
        }
    }
}

module cage_openings(
    length=CAGE_LENGTH-CAGE_WIDTH_BOTTOM,
    width=CAGE_WIDTH_TOP,
    height=CAGE_HEIGHT/2,
    vertical_slit=WALL_THICKNESS,
    vertical_count=CAGE_HEIGHT/WALL_THICKNESS/6,
    horizontal_bar=GRILLE_HORIZONTAL_BAR,
    horizontal_count=GRILLE_HORIZONTAL_COUNT
) {
    object_length = spacing__object_length(length, horizontal_bar, horizontal_count);
    translate([-length/2, -width/2, 0])
    spacing__place_object(height, 0, vertical_count, v=[0, 0, 1]) {
        spacing__place_object(length, horizontal_bar, horizontal_count)
            cube([object_length, width, vertical_slit]);
    }
}

module cage_support_part(width, thickness=GRILLE_HORIZONTAL_BAR)
{
    translate([0, 0, width/2])
    rotate([0, 90, 0])
    linear_extrude(thickness)
    difference() {
        intersection() {
            circle(d=width);
            translate([width/4, 0, 0]) square([width/2, width], center=true);
        }
        difference() {
            translate([0, 0, 0]) rotate([0, 0, 45]) square(width*sin(45), center=true);
            translate([width-1.5*WALL_THICKNESS, 0, 0]) square(width, center=true);
        }
    }
}

module cage_support(
    length=CAGE_LENGTH-CAGE_WIDTH_BOTTOM,
    width=CAGE_WIDTH_BOTTOM,
    count=GRILLE_HORIZONTAL_COUNT
) {
    translate([-length/2, 0, 0.5*WALL_THICKNESS])
    spacing__place_spacer_lr(length, GRILLE_HORIZONTAL_BAR, count)
        cage_support_part(width-0.5*WALL_THICKNESS);
}

module bottom_openings(
    length=CAGE_LENGTH-CAGE_WIDTH_BOTTOM,
    width=CAGE_WIDTH_BOTTOM,
    count=GRILLE_HORIZONTAL_COUNT,
    more=true
) {
    a = 360/16;
    angles = more ? [-a, -a*2, -a*3, -a*5, -a*6, -a*7] : [-a*2, -a*3, -a*5, -a*6];
    translate([-length/2, 0, 0])
    difference() {
        for (a = angles) {
            rotate([a, 0, 0]) translate([length/2, 0, 0])
                cube([length, width+2, WALL_THICKNESS], center=true);
        }
        spacing__place_spacer(length, GRILLE_HORIZONTAL_BAR, count) {
            translate([GRILLE_HORIZONTAL_BAR/2, 0, 0]) rotate([0, 90, 0])
            cylinder(d=width+4, h=GRILLE_HORIZONTAL_BAR, center=true);
        }
        translate([-1, -(width+6)/2, 0]) cube([length+2, width+6, width/2+3]);
    }
}

// ---------------------------------------------------------------

module pot(
    length=CAGE_LENGTH,
    width_top=CAGE_WIDTH_TOP,
    width_bottom=CAGE_WIDTH_BOTTOM,
    height=CAGE_HEIGHT,
    plate_length=CAP_LENGTH,
    plate_width=CAP_WIDTH,
    r=ROUNDING_RADIUS,
    support=SUPPORT_STRUCTURES,
    bottom_openings=BOTTOM_OPENINGS
)
{
    translate([0, 0, WALL_THICKNESS]) rotate([180, 0, 0])
    difference() {
        union() {
            difference() {
                cage_base(length, width_top, width_bottom, height, plate_length, plate_width, r);
                if (bottom_openings!="none") {
                    translate([0, 0, -height+width_bottom/2])
                        bottom_openings(
                            length=length-width_bottom,
                            width=width_bottom,
                            more=bottom_openings=="more"
                        );
                }
            }
            if (support) {
                intersection() {
                    translate([0, 0, -height])
                        cage_support(
                            length=length-width_bottom,
                            width=width_bottom
                        );
                    cage_form(
                        length-WALL_THICKNESS,
                        width_top-WALL_THICKNESS,
                        width_bottom-WALL_THICKNESS,
                        height-WALL_THICKNESS,
                        r=r-WALL_THICKNESS/2
                    );
                }
            }
        }
        translate([0, 0, -(height-width_bottom/2)])
            cage_openings(
                length=length-width_bottom,
                width=width_top,
                height=height/2,
                vertical_count=height/WALL_THICKNESS/6
            );
    }
}

module blank_cap(
    length=CAGE_LENGTH,
    width_top=CAGE_WIDTH_TOP,
    width_bottom=CAGE_WIDTH_BOTTOM,
    height=CAP_WIDTH,
    plate_length=CAP_LENGTH,
    plate_width=CAP_WIDTH,
    r=ROUNDING_RADIUS
)
{
    rotate([180, 0, 0])
    translate([0, 0, -WALL_THICKNESS/2])
    union() {
        rounded_plate(length=plate_length, width=plate_width, height=WALL_THICKNESS);
        cage_form(length, width_top, width_bottom, height, r);
    }
}

// ---------------------------------------------------------------

if (OBJECT_TO_PRINT=="cap") {
    blank_cap($fn=ROUNDNESS);
} else {
    pot($fn=ROUNDNESS);
}
